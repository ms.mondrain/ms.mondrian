# Frontend New Hires Resources

  Personally, I find the sheer amount of documentations we have to be daunting for new hires to go through. And I started to have a sizable collection of bookmarks which I have troubles managing.

  So I put together a list of links that people send me in slack DMs, code review comments and etc:

## Coding

- Various frontend style guides:
  - [vue.js](https://docs.gitlab.com/ee/development/fe_guide/style/vue.html)
  - [vue on docs.gitlab.com](https://docs.gitlab.com/ee/development/fe_guide/vue.html)
  - [scss](https://docs.gitlab.com/ee/development/fe_guide/style/scss.html) with special attention on [utility classes](https://docs.gitlab.com/ee/development/fe_guide/style/scss.html#utility-classes)
- [Implementing EE features](https://docs.gitlab.com/ee/user/project/quick_actions.html)
- [Internationalization](https://docs.gitlab.com/ee/development/i18n/externalization.html)
- [Changelog entries](https://docs.gitlab.com/ee/development/changelog.html) and [handling changelog with feature flags](https://docs.gitlab.com/ee/development/feature_flags/index.html#changelog)

## Dev Workflow Tip

- Use [`glab mr checkout`](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/) to checkout branches when reviewing MRs

## Documentations

- [Recommended word list](https://docs.gitlab.com/ee/development/documentation/styleguide/word_list.html)
- [Punctuation overview](https://design.gitlab.com/content/punctuation/#punctuation-overview)

## GitLab, the product

- [GitLab quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html)
