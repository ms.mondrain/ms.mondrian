# On Ruby related things

## `do end` (blocks) are like arrow functions in javascript

```ruby
within_testId('status-checks-table') do

end
```

is the same as

```javascript
withinTestId('status-checks-table', () => {})

```

the definition of `within_testId` looks like this

```ruby
  def within_testId(testid, context: page, **kwargs, &block)
    context.within("[data-testid='#{testid}]", **kwargs, &block)
```
