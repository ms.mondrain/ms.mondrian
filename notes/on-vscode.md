# On vscode related things

## search inside vscode

first of all, the most right button turn the search in regex mode.
![screenshot of vscode search UI](./screenshots/vs-code-search.png)

- `visit.*branch_rules`
![search results of visit.*branch_rules](./screenshots/search-visit-branch-rule.png)

- `create.?status.?check`with upper/lower casing ignored will show all the casing
![search results of createStatusCheck with all the casing](./screenshots//vs-code-search-all-the-casing.png)
