# Things that don't fit other pages

## use a `i18n` property on the component `$options` object

even though it's attempting to move the same string into a `constant.js` file but
this creates a layer of complexity. for spec, it can creates false positive so it's
recommended to test with the actual copy. see [docs](https://docs.gitlab.com/ee/development/i18n/externalization.html)
