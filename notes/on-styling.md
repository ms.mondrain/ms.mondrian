# On styling related things

## Avoid having outer margins on component root element

[This article](https://kyleshevlin.com/no-outer-margin/) explains some of the details. See [example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160804#note_2017902712)

## Add horizontal margin with `mx` and vertical one with `my`

[Tailwind](https://tailwindcss.com/docs/margin#add-horizontal-margin) offers these to add margins on left AND right, top AND bottom.
