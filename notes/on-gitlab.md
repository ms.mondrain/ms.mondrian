# on GitLab product features

## [Source Editor](https://docs.gitlab.com/ee/development/fe_guide/source_editor.html)

Extension of Monaco editor. Multiple features, such as Web IDE, CI Linter, Snippets, Web Editor and Security Policies use it.

Use only when users need to edit the file. For view only, use `BlobContent` Component.
