# On testing related things

## prefer vm.$emit over trigger when using child components

Overall the rule is simple: you deal with "real HTML element" and you know it - trigger.
You deal with Vue component - emit. [example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160804#note_2026039850)

## use `.emitted($event-name)`

instead of `emitted()[$event-name]` for better error messages. [example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160804#note_2026054759)

## rspec

### feature flag in rspec

All feature flags are set to be `true` in the tests. If we want to test a scenario of flag off, we will override in the test to target flag off.

### debug a rspec in a browser window

use `live_debug`!

### sharing set up with variable in rspec

in `before_do`add the variable with `{ VARIABLE }`and in each test, set the variable with `let {VARIABLE} {VALUE}`. [example](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/163237#note_2079105866)
