My name is **Chao** (pronunciation ‘ciao’). I go by the first letter of my first name, Chaoyue. I am currently a Frontend Engineer at GitLab, specifically the Source Code group in the Create Stage. This README is my work portrait that tells you a little bit about the work me.

## 🖼️ About the handle  
My first ever Javascript project, when I was in coding bootcamp, was a [Mondrian generator](https://chaoyue-zhao.github.io/mondrian-in-style/). One of my friends who writes for films mentioned “ms.mondrian” would make a dope screen name. That has stuck with me ever since. My dream is to have someone paint their wall with a mondrian generated from my app. 

## 🥳 Fun Facts 
- I am an only child.
- I was born and raised in Beijing, China and came to Canada for university when I was 19. I lived in Ottawa for 6 years, before moving to Toronto. 
- I am a software developer without a CS degree. I studied Communication Studies and Psychology. This was my undergraduate thesis research: [Operand-order Specificity in Single-digit Multiplication and Addition: An Eye Moment Study of Chinese-educated Adults](https://www.youtube.com/watch?v=KDPuPuJParc). 
In a previous life, I planned B2B events for living. 
- I am a [ENFJ](https://www.16personalities.com/enfj-personality). 
- **POTTERY IS LIFE**.  
- I wrote a poem in Jan 2024 and I thought it was fun: 

  >>>
  I wish us endless cool product feature dreams and the furious desire to realize some of them.\
  I wish us the eagle eyes to track down those bugs, before they make it to staging.\
  I wish us to respect the differences of ourselves, because the worth and virtues of each person often remain to be discovered.\ 
  I wish us at last to never give up the search for what work means for us holistically.\ 
  I wish us above all the comfort to show up as our true selves and the damn good vibe.
  >>>

## 🥅 Goals:
### 1 year goal: 
Get promoted to senior or on my way to there.
### Long term goal (5 years+): 
Currently I would like to explore the people management route as I really enjoy mentorship. I like to help people find their niche. 

## 🦸🏻‍♀️ Superpowers
- **Communication**: I value both synchronous and asynchronous communication, understand the subtle differences between the two and constantly push myself and others to properly leverage both. I care and write technical documentations. I learn in public by sharing my learnings formally and informally. 
- **Empathy**: I develop a great sense of empathy through my undergraduate degree in communication & psychology and my past career as an event manager. I celebrate the diversity in points of views folks bring to the table, based on their own experience and expertise. 
- **Humility**: I once was told to take work seriously, not as myself. I believe, no matter how many years of experience one has on anything, there is always room for curiosity. 

## 🙏 Weaknesses and how you can help me 😊
- **Patience**: I am learning things that take time, especially the better ones. The best time could be now, but more often than not, it’s a bit later. Please help me to slow down and give space for others. 
- **Working in a larger company**: I am new to GitLab and companies this size. I am learning my way around navigating a large code base and things on this scale. Please share what you know about GitLab (company, product, code base, culture and beyond).
- **Overcommitting**: I’ve got this feedback often. I am consciously prioritizing what I can do myself vs delegate. Please remind me to do this, if you see the chance to do so. 

## 👠 Work Style
### Dev Day to Day
- I prioritize code reviews over my own work, unless I am putting out production fire. My code review is extremely detailed. I pick apart every single line change. I ask a lot of questions. I will use `nit` to signify, which can be left to the discretion of the author. I provide a high level summary with actionable in the code review summary itself. 
- I use Git history a lot to find a person to unblock myself. 
- I choose [boring solutions](https://handbook.gitlab.com/handbook/values/#boring-solutions). I consider clever code harmful. In other words, readability for the win. 
- I aim to not be a bus factor on anything. Sharing is caring. 
- I do well and don’t mind them. But I want to spend most of my time, **not** [being a glue](https://noidea.dog/glue). 

### Asynchronous vs Synchronous Communication 
- I default to ask questions and start conversations in public slack channels. I personally believe that it speeds up getting an answer while promoting inclusivity and transparency. 
- My ideal workday is a nice balance of individual head down time and synchronous collaboration with the team. 
- I prefer slack messages **first** and then in person/google meets/zoom/huddle chats so I can have context for our conversations. 
- I am a heavy emoji user. I would use emojis to say thank you or acknowledge I have read a message, instead of typing out a message. 
- I tend to need time to process information before providing answers.

### Meetings 
- If possible, please avoid booking me on **Mondays**, **Wednesdays** and **Fridays** as I like to leave out 2 days a week to be focus time only . 
- I care a lot about meeting hygiene. 
   - I prefer to go to meetings with an agenda/ a list of things that we hope to achieve/ pre-reads / pre-work and a clear invite list. 
   - I would like to see meetings open up with a clear understanding of what hope the outcomes will be.  
   - After them, a second out a meeting notes, action items and owners.  

### Work Philosophy 
- Value excellent user experience for all, developer experience and build to last. 
- Productivity for me means spending time intentionally, effectively and enjoyable.  
- I am in the high growth period of my career so I want to do work that “set the bar high, just out of reach”, knowing there are people who would support me. 
- I thrive on improving the process so we can all work better together. 
- I find it’s hard to proceed when people stop caring. 
- I like my boundaries to be respected and I try my best to respect others’. 
- I like to push for repeated processes to get automated and templatize.  

### Others
- I like to be guided to find my own answers vs being fed directly what to do. 
- I tend to talk and think and can go on tangents. I am open to being told that I am running off-topic.
- I believe one can’t listen when they talk or wait for their chance to speak. 
- I like to receive feedback on a timely basis and I LOVE them, positive or constructive. - I like to receive constructive feedback with potential discussion afterwards on how to do better. 
- I like to receive recognition/kudos publicly.  
- I love PEOPLE. Crowds energize me. Even though I am extremely extroverted, I need my me time too. 


## 🤝 Preferred management style 
- For me, a good EM understands the role is very different from coding/ individual contributors. EM wears many many different hats. Some of these includes:  
    - Coach 
    - Cheerleader 
    - Mentor
    - Party planner
    - Peacemaker
    - Shot caller
    - Speaker of the house 
    - Notetaker
    - Project manager
    - Salesperson (sell the team to other stakeholders)
- For me, A good EM knows everything is built on trust. Trusting the good people you hire can do the job and do a great job on their own and GET OUT OF THE WAY.  
- For me, A good EM is a [Vulnerable leader](https://www.cultureamp.com/blog/brene-brown-vulnerability-in-leadership). It’s the person who admits what they don’t know, apologizes publicly and understands “work is important, but it’s not one whole self”.
- For me, A good EM understands the importance of 1:1s and uses them as opportunities for ongoing career conversations.
